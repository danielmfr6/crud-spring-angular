import { Cliente } from './cliente';
import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';  
import { Observable } from 'rxjs';  

@Injectable({
  providedIn: 'root'
})

export class ClienteService {

  private url = 'http://localhost:8080/cliente';
  constructor(private http:HttpClient) {}

   getClienteList(): Observable<any> {
    return this.http.get(`${this.url}`);
  }

  createCliente(cliente: object): Observable<object> {
    return this.http.post(`${this.url}`, cliente);
  }

  deleteCliente(id: number): Observable<any> {
    return this.http.delete(`${this.url}/${id}`)
  }

  getCliente(id: number): Observable<any> {
    return this.http.get(`${this.url}/${id}`);
  }

  updateCliente(id: number, cliente: Cliente): Observable<any>{
    return this.http.put(`${this.url}/${id}`, cliente);
  }
}
