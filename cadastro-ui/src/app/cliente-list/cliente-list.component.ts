import { Observable, Subject } from 'rxjs';
import { ClienteService } from './../cliente.service';
import { Component, OnInit } from '@angular/core';
import { Cliente } from '../cliente';
import { Router } from '@angular/router';


import {FormControl,FormGroup,Validators} from '@angular/forms';  


@Component({
  selector: 'app-cliente-list',
  templateUrl: './cliente-list.component.html',
  styleUrls: ['./cliente-list.component.css']
})
export class ClienteListComponent implements OnInit {

  constructor(private clienteService:ClienteService, private router:Router) { }

  clientesArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();

  clientes: Observable<Cliente[]>;
  cliente: Cliente = new Cliente();
  deleteMessage = false;
  clienteList:any;
  isUpdated = false;

  ngOnInit() {
    this.isUpdated = false;
    this.dtOptions = {
      pageLength: 6,
      stateSave: true,
      lengthMenu: [[6,16,20,-1], [6,16,20,"All"]],
      processing: true
    };
    this.clienteService.getClienteList().subscribe(data => {
      this.clientes = data;
      this.dtTrigger.next();
    });
  }

  deleteCliente(id: number){
    this.clienteService.deleteCliente(id)
      .subscribe( data => {
        console.log(data);
        this.deleteMessage = true;
        this.clienteService.getClienteList().subscribe(data => {
          this.clientes = data
        });
      }, error => console.log(error));
  }

  clienteUpdateForm = new FormGroup({
    id: new FormControl(),
    razaoSocial: new FormControl(),
    cnpj: new FormControl(),
    telefone: new FormControl()
  });

  updateCliente(cliente:Cliente){
    localStorage.setItem("id", cliente.id.toString());
    this.router.navigate(["edit-cliente"]);
  }

  get idCliente(){
    return this.clienteUpdateForm.get('id');
  }

  get razaoSocialCliente(){
    return this.clienteUpdateForm.get('razaoSocial');
  }

  get cnpjCliente(){
    return this.clienteUpdateForm.get('cnpj');
  }

  get telefoneCliente(){
    return this.clienteUpdateForm.get('telefone');
  }

  changeisUpdate(){  
    this.isUpdated= true;  
  }  

}
