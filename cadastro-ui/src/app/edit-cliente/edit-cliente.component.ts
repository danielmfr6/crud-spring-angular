import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClienteService } from './../cliente.service';
import { Cliente } from '../cliente';

@Component({
  selector: 'app-cliente-edit',
  templateUrl: './edit-cliente.component.html',
  styleUrls: ['./edit-cliente.component.css']
})
export class EditClienteComponent implements OnInit {

  cliente :Cliente = new Cliente();
  constructor(private router:Router,private service:ClienteService) { }

  ngOnInit() {
    this.updateCliente();
  }

  updateCliente(){
    let id=localStorage.getItem("id");
    this.service.getCliente(+id)
    .subscribe(data=>{
      this.cliente=data;
    })

  }

  Atualizar(cliente:Cliente){
    this.service.updateCliente(cliente.id,cliente)
      .subscribe(data => {
        this.cliente = data;
        alert("Dados atualizados com sucesso");
        this.router.navigate(["view-cliente"]);
      });
  }


}