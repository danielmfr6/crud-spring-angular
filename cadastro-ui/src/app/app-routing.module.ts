import { AddClienteComponent } from './add-cliente/add-cliente.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditClienteComponent } from './edit-cliente/edit-cliente.component';

const routes: Routes = [
  { path: '', redirectTo: 'view-student', pathMatch: 'full' },
  { path: 'view-cliente', component: ClienteListComponent},
  { path: 'add-cliente', component: AddClienteComponent },
  { path: 'edit-cliente', component: EditClienteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
