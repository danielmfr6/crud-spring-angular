import { Component, OnInit } from '@angular/core';
import { ClienteService } from './../cliente.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Cliente } from '../cliente';

@Component({
  selector: 'app-add-cliente',
  templateUrl: './add-cliente.component.html',
  styleUrls: ['./add-cliente.component.css']
})
export class AddClienteComponent implements OnInit {

  cliente: Cliente=new Cliente();
  submitted = false;

  constructor(private clienteService: ClienteService) { }

  ngOnInit() {
    this.submitted = false;
  }

  clientesaveForm = new FormGroup({
    razaoSocial: new FormControl("", [Validators.required, Validators.minLength(5)]),
    cnpj: new FormControl("", [Validators.required, Validators.minLength(14), Validators.maxLength(14)]),
    telefone: new FormControl("", [Validators.required, Validators.minLength(10), Validators.maxLength(11)])
  })

  saveCliente(saveCliente){
    this.cliente = new Cliente();
    this.cliente.razaoSocial = this.razaoSocialCliente.value;
    this.cliente.cnpj = this.cnpjCliente.value;
    this.cliente.telefone = this.telefoneCliente.value;
    this.submitted = true;

    this.save();
  }

  save(){
    this.clienteService.createCliente(this.cliente)
      .subscribe(data => console.log(data), error => console.log(error));
    this.cliente = new Cliente;
  }

  get razaoSocialCliente(){
    return this.clientesaveForm.get('razaoSocial');
  }

  get cnpjCliente(){
    return this.clientesaveForm.get('cnpj');
  }

  get telefoneCliente(){
    return this.clientesaveForm.get('telefone');
  }

  addClienteForm(){
    this.submitted=false;
    this.clientesaveForm.reset();
  }


}
