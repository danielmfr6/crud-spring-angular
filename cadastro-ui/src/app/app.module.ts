import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {DataTablesModule} from 'angular-datatables';  
import { AppComponent } from './app.component';

import { AddClienteComponent } from './add-cliente/add-cliente.component';
import { ClienteListComponent } from './cliente-list/cliente-list.component';
import { EditClienteComponent } from './edit-cliente/edit-cliente.component';

@NgModule({
  declarations: [
    AppComponent,
    AddClienteComponent,
    ClienteListComponent,
    EditClienteComponent
  ],
  imports: [
    BrowserModule,  
    AppRoutingModule,  
    FormsModule,  
    ReactiveFormsModule,  
    HttpClientModule,  
    DataTablesModule  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
