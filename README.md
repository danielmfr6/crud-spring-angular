# Angular-Spring-Crud

O objetivo deste projeto é criar um CRUD App, no qual criamos a API com Spring Boot e o lado Cliente para consumir a API com Angular

# Como rodar

Database

Acesse o MySQL workbench e siga os passos
- Crie um banco de dados com o nome 'testedb'
- Cria um Usuário para esse Schema com as mesmas credenciais encontradas em cadastroapi/src/resources/application.properties

Client-side
- Vá até a pasta cadastro-ui e digite os comandos abaixo
- npm install
- npm start

Server-side

Você pode rodar a aplicação REST via linha de comando com Maven
Entre na pasta 'cadastroapi' e digite:

- Usando o Maven 
./mvnw spring-boot:run

