package com.danielramos.cadapp.api.cadastroapi.infrastructure.web.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.danielramos.cadapp.api.cadastroapi.domain.cliente.Cliente;
import com.danielramos.cadapp.api.cadastroapi.domain.cliente.ClienteRepository;

@CrossOrigin(origins = "*",maxAge = 3600)
@RestController
@RequestMapping("/cliente")
public class ClienteControler {

	@Autowired
	private ClienteRepository clienteRepository;
	
	@PostMapping
	public Cliente adicionar(@Valid @RequestBody Cliente cliente) {
		return clienteRepository.save(cliente);
	}
	
	@GetMapping
	public List<Cliente> listar(){
		return clienteRepository.findAll();
	}
	
	@GetMapping("/{id}") // /contatos/?
	public ResponseEntity<Cliente> buscar(@PathVariable Integer id){
		Cliente cliente = clienteRepository.findById(id).orElseThrow();;
		
		if(cliente == null) {
			return ResponseEntity.notFound().build(); // Retorna 404
		}
		
		return ResponseEntity.ok(cliente);
	}
	
	@CrossOrigin(origins = "http://localhost:4200/edit-cliente/",maxAge = 3600)
	@PutMapping("/{id}")
	public ResponseEntity<?> atualizar(@PathVariable Integer id, @Valid @RequestBody Cliente cliente){
		Cliente exist = clienteRepository.findById(id).orElseThrow();
		if(exist == null) {
			return ResponseEntity.notFound().build();
		}
		BeanUtils.copyProperties(cliente, exist, "id");
		
		exist = clienteRepository.save(exist);
		
		return ResponseEntity.ok(exist);	
		
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> remover(@PathVariable Integer id){
		Cliente cliente = clienteRepository.findById(id).orElseThrow();
		
		if(cliente == null) {
			return ResponseEntity.notFound().build();
		}
		
		clienteRepository.delete(cliente);
		return ResponseEntity.noContent().build(); // Return 204
	
	}
	
}
