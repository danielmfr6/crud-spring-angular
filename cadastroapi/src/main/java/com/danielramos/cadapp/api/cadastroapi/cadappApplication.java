package com.danielramos.cadapp.api.cadastroapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class cadappApplication {

	public static void main(String[] args) {
		SpringApplication.run(cadappApplication.class, args);
	}

}
